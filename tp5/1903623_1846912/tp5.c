#include <stdio.h>

unsigned int Decryption_fct(unsigned int le)
{
	unsigned int be = 0;

	/*
	 * Remplacez le code suivant par de l'assembleur en ligne
	 * en utilisant le moins d'instructions possible
	*/
	// be = (le & 0xff000000) | (le&0xff) << 16  | (le & 0xff00) | (le & 0xff0000) >> 16;
	

	asm volatile(
		"movl %1, %%eax\n\t"
		"roll $16, %%eax\n\t"
		"andl $0x00FF00FF, %%eax\n\t"
		"andl $0xFF00FF00, %1\n\t"
		"orl %1, %%eax\n\t"
		"movl %%eax, %0"
		
		: "=q"(be)// sorties (s'il y a lieu)
		: "g"(le)// entrées
		: "eax"// registres modifiés (s'il y a lieu)
	);

	return be;
}

int main()
{
	unsigned int data = 0xeeaabbff;

	printf("Représentation crypte en little-endian:   %08x\n"
	       "Représentation decrypte en big-endian:    %08x\n",
	       data,
	       Decryption_fct(data));

	return 0;
}
